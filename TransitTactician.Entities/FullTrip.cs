using GTFS.Entities;
using TransitRealtime;

namespace TransitTactician.Entities;

public class FullTrip
{
    public StopTime departureStopTime { get; set; }
    public Stop departureStop { get; set; }
    public StopTime arrivalStopTime { get; set; }
    public Stop arrivalStop { get; set; }
    public Trip trip { get; set; }
    public Route route { get; set; }
    public CalendarDate calendarDate { get; set; }
    public FeedEntity feedEntity { get; set; }
    public int predictedLateness { get; set; }
    
    public FullTrip(StopTime departureStopTime, Stop departureStop, StopTime arrivalStopTime, Stop arrivalStop, Trip trip, Route route, CalendarDate calendarDate, FeedEntity feedEntity, int predictedLateness)
    {
        this.departureStopTime = departureStopTime;
        this.departureStop = departureStop;
        this.arrivalStopTime = arrivalStopTime;
        this.arrivalStop = arrivalStop;
        this.trip = trip;
        this.route = route;
        this.calendarDate = calendarDate;
        this.feedEntity = feedEntity;
        this.predictedLateness = predictedLateness;
    }
}