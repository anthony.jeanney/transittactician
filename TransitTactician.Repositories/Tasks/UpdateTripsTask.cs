using System.Net;
using GTFS;
using GTFS.Entities;
using Hangfire;
using ProtoBuf;
using TransitRealtime;

namespace TransitTactician.Repositories.Tasks;

public sealed class UpdateTripsTask
{
    private static UpdateTripsTask Instance = null;
    
    // GTFS
    private List<Trip> _trips;
    private List<Route> _routes;
    private List<Stop> _stops;
    private List<StopTime> _stopTimes;
    private List<Agency> _agencies;
    private List<CalendarDate> _calendarDates;
    
    // GTFS-RT
    private List<FeedEntity> _gtfsRtEntities;
    
    public static UpdateTripsTask GetInstance()
    {
        if (Instance == null)
        {
            Instance = new UpdateTripsTask();
        }
        return Instance;
    }
    
    private UpdateTripsTask()
    {
        _trips = new List<Trip>();
        _routes = new List<Route>();
        _stops = new List<Stop>();
        _stopTimes = new List<StopTime>();
        _agencies = new List<Agency>();
        _calendarDates = new List<CalendarDate>();
        
        // run at start
        FetchTerGtfs();
        FetchTerGtfsRt();
        IRecurringJobManager recurringJobManager = new RecurringJobManager();
        // run every 24 hours
        recurringJobManager.AddOrUpdate("FetchTerGtfsJob", () => FetchTerGtfsJob.Run(), Cron.Daily);
        // run every 2 minutes
        recurringJobManager.AddOrUpdate("FetchTerGtfsRtJob", () => FetchTerGtfsRtJob.Run(), "*/2 * * * *");
    }

    public async Task FetchTerGtfs()
    {
        // GTFS (Update every 24h) (Contains all the data for TER trains for the next 60 days)
        
        // https://eu.ftp.opendatasoft.com/sncf/gtfs/export-ter-gtfs-last.zip

        // download the file
        var client = new HttpClient();
        var stream = await client.GetStreamAsync("https://eu.ftp.opendatasoft.com/sncf/gtfs/export-ter-gtfs-last.zip");

        // save the zip file
        var zipPath = Path.Combine(Directory.GetCurrentDirectory(), "export-ter-gtfs-last.zip");
        using (var fileStream = File.Create(zipPath))
        {
            await stream.CopyToAsync(fileStream);
        }

        // create the reader
        var reader = new GTFSReader<GTFSFeed>();
        var feed = reader.Read(zipPath);

        // save the data
        _trips = feed.Trips.ToList();
        _routes = feed.Routes.ToList();
        _stops = feed.Stops.ToList();
        _stopTimes = feed.StopTimes.ToList();
        _agencies = feed.Agencies.ToList();
        _calendarDates = feed.CalendarDates.ToList();
        
        Console.WriteLine(
            $"Fetched TER GTFS Data! : {_trips.Count} trips, {_routes.Count} routes, {_stops.Count} stops, {_stopTimes.Count} stop times, {_agencies.Count} agencies");
        
        // TODO Link all of that to the API
        
        // Usage example
        // Filter trips
        /*var trips = feed.Trips
            .Select(trip => new
            {
                Trip = trip,
                Routes = feed.Routes.Where(x => x.Id == trip.RouteId),
                CalendarDate = feed.CalendarDates.First(x => x.ServiceId == trip.ServiceId)
                //StopTimes = feed.StopTimes.Where(x => x.TripId == trip.Id).OrderBy(x => x.StopSequence),
                //Stop = feed.Stops.First(x => x.Id == feed.StopTimes.First(y => y.TripId == trip.Id).StopId)
            })
            .Where(x => x.Routes.Any(x => x.LongName.Contains("Calais")) && x.CalendarDate.Date >= DateTime.Now.Date &&
                        x.CalendarDate.Date <= DateTime.Now.AddDays(1).Date);*/

        // Trip with specific id
        /*var trips = feed.Trips
            .Select(trip => new
            {
                Trip = trip,
                CalendarDate = feed.CalendarDates.First(x => x.ServiceId == trip.ServiceId)
            })
            .Where(x => x.Trip.Id == "OCESN857522F2636196:2024-01-07T00:46:05Z")
            .OrderBy(x => x.CalendarDate.Date);*/

        /*foreach (var trip in trips)
        {
            Console.WriteLine($"\nTrip {trip.Trip.Id} : RouteId:{trip.Trip.RouteId}, ServiceId:{trip.Trip.ServiceId}, Headsign:{trip.Trip.Headsign}, ShortName:{trip.Trip.ShortName}, DirectionId:{trip.Trip.Direction}, BlockId:{trip.Trip.BlockId}, ShapeId:{trip.Trip.ShapeId}");
            Console.WriteLine($"CalendarDate : ServiceId:{trip.CalendarDate.ServiceId}, Date:{trip.CalendarDate.Date}, ExceptionType:{trip.CalendarDate.ExceptionType}");
            
            var route = feed.Routes.First(x => x.Id == trip.Trip.RouteId);
            Console.WriteLine($"Route {route.Id} : AgencyId:{route.AgencyId}, ShortName:{route.ShortName}, LongName:{route.LongName}, Type:{route.Type}, Color:{route.Color}, TextColor:{route.TextColor}");
            
            var stopTimes = feed.StopTimes.Where(x => x.TripId == trip.Trip.Id).OrderBy(x => x.StopSequence);
            foreach (var stopTime in stopTimes)
            {
                var stop = feed.Stops.First(x => x.Id == stopTime.StopId);
                Console.WriteLine($"Stop {stop.Id} : Name:{stop.Name}, ArrivalTime:{stopTime.ArrivalTime}, DepartureTime:{stopTime.DepartureTime}, Latitude:{stop.Latitude}, Longitude:{stop.Longitude}, LocationType:{stop.LocationType}, ParentStation:{stop.ParentStation}, StopSequence:{stopTime.StopSequence}");
            }
        }*/
        
        // Console.WriteLine("\nDone!");
    }

    public async Task FetchTerGtfsRt()
    {
        // GTFS-RT (Update every 2min for trains scheduled for the next 60 minutes)
        WebRequest req =
            HttpWebRequest.Create("https://proxy.transport.data.gouv.fr/resource/sncf-ter-gtfs-rt-trip-updates");
        FeedMessage feed = Serializer.Deserialize<FeedMessage>(req.GetResponse().GetResponseStream());
        
        _gtfsRtEntities = feed.Entities.ToList();

        Console.WriteLine(
            $"Fetched TER GTFS-RT Data! : {_gtfsRtEntities.Count} entities");

        // FeedEntity
        /*foreach (FeedEntity entity in _gtfsRtEntities)
        {
            // TripUpdate
            TripUpdate tripUpdate = entity.TripUpdate;
            Console.WriteLine(
                $"TripUpdate: {tripUpdate.Timestamp} (Timestamp), {tripUpdate.Delay} (Delay)");
            TripDescriptor trip = tripUpdate.Trip;
            Console.WriteLine(
                $"  TripDescriptor: {trip.TripId} (TripId), {trip.RouteId} (RouteId), {trip.DirectionId} (DirectionId), {trip.StartTime} (StartTime), {trip.StartDate} (StartDate), {trip.schedule_relationship} (schedule_relationship)");

            VehicleDescriptor vehicle = tripUpdate.Vehicle;
            if (vehicle != null)
            {
                Console.WriteLine(
                    $"  VehicleDescriptor: {vehicle.Id} (Id), {vehicle.Label} (Label), {vehicle.LicensePlate} (LicensePlate)");
            }

            List<TripUpdate.StopTimeUpdate> stopTimeUpdates = tripUpdate.StopTimeUpdates;
            foreach (TripUpdate.StopTimeUpdate stopTimeUpdate in stopTimeUpdates)
            {
                TripUpdate.StopTimeEvent arrival = stopTimeUpdate.Arrival;
                TripUpdate.StopTimeEvent departure = stopTimeUpdate.Departure;
                Console.WriteLine(
                    $"  StopTimeUpdate: {stopTimeUpdate.StopSequence} (StopSequence), {stopTimeUpdate.StopId} (StopId), {stopTimeUpdate.schedule_relationship} (schedule_relationship)");
                if (arrival != null)
                {
                    Console.WriteLine(
                        $"    StopTimeEvent (Arrival): {arrival.Delay} (Delay), {arrival.Time} (Time), {arrival.Uncertainty} (Uncertainty)");
                }

                if (departure != null)
                {
                    Console.WriteLine(
                        $"    StopTimeEvent (Departure): {departure.Delay} (Delay), {departure.Time} (Time), {departure.Uncertainty} (Uncertainty)");
                }
            }

            // VehiclePosition
            VehiclePosition vehiclePosition = entity.Vehicle;
            if (vehiclePosition != null)
            {
                Console.WriteLine(
                    $"VehiclePosition: {vehiclePosition.CurrentStopSequence} (CurrentStopSequence), {vehiclePosition.StopId} (StopId), {vehiclePosition.Timestamp} (Timestamp), {vehiclePosition.CurrentStatus} (CurrentStatus), {vehiclePosition.congestion_level} (congestion_level), {vehiclePosition.occupancy_status} (occupancy_status)");
                TripDescriptor trip2 = vehiclePosition.Trip;
                Console.WriteLine(
                    $"  TripDescriptor: {trip2.TripId} (TripId), {trip2.RouteId} (RouteId), {trip2.DirectionId} (DirectionId), {trip2.StartTime} (StartTime), {trip2.StartDate} (StartDate), {trip2.schedule_relationship} (schedule_relationship)");
                VehicleDescriptor vehicle2 = vehiclePosition.Vehicle;
                Console.WriteLine(
                    $"  VehicleDescriptor: {vehicle2.Id} (Id), {vehicle2.Label} (Label), {vehicle2.LicensePlate} (LicensePlate)");
                Position position = vehiclePosition.Position;
                Console.WriteLine(
                    $"  Position: {position.Latitude} (Latitude), {position.Longitude} (Longitude), {position.Bearing} (Bearing), {position.Odometer} (Odometer), {position.Speed} (Speed)");
            }

            // Alert
            Alert alert = entity.Alert;
            if (alert != null)
            {
                Console.WriteLine(
                    $"Alert: {alert.cause} (cause), {alert.effect} (effect), {alert.Url} (Url), {alert.HeaderText} (HeaderText), {alert.DescriptionText} (DescriptionText)");
                List<TimeRange> activePeriods = alert.ActivePeriods;
                foreach (TimeRange activePeriod in activePeriods)
                {
                    Console.WriteLine(
                        $"  TimeRange: {activePeriod.Start} (Start), {activePeriod.End} (End)");
                }

                List<EntitySelector> informedEntities = alert.InformedEntities;
                foreach (EntitySelector informedEntity in informedEntities)
                {
                    Console.WriteLine(
                        $"  InformedEntity: {informedEntity.AgencyId} (AgencyId), {informedEntity.RouteId} (RouteId), {informedEntity.RouteType} (RouteType), {informedEntity.StopId} (StopId)");
                    TripDescriptor trip3 = informedEntity.Trip;
                    Console.WriteLine(
                        $"    TripDescriptor: {trip3.TripId} (TripId), {trip3.RouteId} (RouteId), {trip3.DirectionId} (DirectionId), {trip3.StartTime} (StartTime), {trip3.StartDate} (StartDate), {trip3.schedule_relationship} (schedule_relationship)");
                }
            }

            Console.WriteLine("");
        }*/
    }
    
    public List<Trip> GetTrips()
    {
        return _trips;
    }
    
    public List<Route> GetRoutes()
    {
        return _routes;
    }
    
    public List<Stop> GetStops()
    {
        return _stops;
    }
    
    public List<StopTime> GetStopTimes()
    {
        return _stopTimes;
    }
    
    public List<Agency> GetAgencies()
    {
        return _agencies;
    }
    
    public List<CalendarDate> GetCalendarDates()
    {
        return _calendarDates;
    }
    
    public List<FeedEntity> GetGtfsRtEntities()
    {
        return _gtfsRtEntities;
    }
}

public static class FetchTerGtfsJob
{
    public static void Run()
    {
        UpdateTripsTask.GetInstance().FetchTerGtfs();
    }
}

public static class FetchTerGtfsRtJob
{
    public static void Run()
    {
        UpdateTripsTask.GetInstance().FetchTerGtfsRt();
    }
}