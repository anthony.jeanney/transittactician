using GTFS.Entities;
using TransitRealtime;
using TransitTactician.Repositories.Tasks;
using TransitTactitian.Core.Interfaces.Repositories;

namespace TransitTactician.Repositories;

public class TripRepository : ITripRepository
{
    private readonly UpdateTripsTask _updateTripsTask;
    
    public TripRepository()
    {
        _updateTripsTask = UpdateTripsTask.GetInstance();
    }
    
    public IEnumerable<Trip> GetTrips()
    {
        return _updateTripsTask.GetTrips();
    }
    
    public Trip GetTripById(string tripId)
    {
        return _updateTripsTask.GetTrips().Where(t => t.Id == tripId).FirstOrDefault();
    }
    
    public IEnumerable<Route> GetRoutes()
    {
        return _updateTripsTask.GetRoutes();
    }
    
    public Route GetRouteById(string routeId)
    {
        return _updateTripsTask.GetRoutes().Where(r => r.Id == routeId).FirstOrDefault();
    }
    
    public IEnumerable<Stop> GetStops()
    {
        return _updateTripsTask.GetStops();
    }
    
    public Stop GetStopById(string stopId)
    {
        return _updateTripsTask.GetStops().Where(s => s.Id == stopId).FirstOrDefault();
    }
    
    public IEnumerable<Stop> GetStopsByName(string stopName)
    {
        return _updateTripsTask.GetStops().Where(s => s.Name.ToLower().Contains(stopName.ToLower())).ToList();
    }
    
    public IEnumerable<StopTime> GetStopTimes()
    {
        return _updateTripsTask.GetStopTimes();
    }
    
    public IEnumerable<StopTime> GetStopTimeByTripId(string tripId)
    {
        return _updateTripsTask.GetStopTimes().Where(st => st.TripId == tripId).ToList();
    }
    
    public IEnumerable<StopTime> GetStopTimeByStopId(string stopId)
    {
        return _updateTripsTask.GetStopTimes().Where(st => st.StopId == stopId).ToList();
    }
    
    public IEnumerable<Agency> GetAgencies()
    {
        return _updateTripsTask.GetAgencies();
    }
    
    public Agency GetAgencyById(string agencyId)
    {
        return _updateTripsTask.GetAgencies().Where(a => a.Id == agencyId).FirstOrDefault();
    }
    
    public IEnumerable<CalendarDate> GetCalendarDates()
    {
        return _updateTripsTask.GetCalendarDates();
    }
    
    public IEnumerable<CalendarDate> GetCalendarDateById(string serviceId)
    {
        return _updateTripsTask.GetCalendarDates().Where(cd => cd.ServiceId == serviceId);
    }
    
    public IEnumerable<FeedEntity> GetFeedEntities()
    {
        return _updateTripsTask.GetGtfsRtEntities();
    }
    
    public FeedEntity GetFeedEntityById(string feedEntityId)
    {
        return _updateTripsTask.GetGtfsRtEntities().Where(fe => fe.Id == feedEntityId).FirstOrDefault();
    }
    
    public int GetPredictedLateness(string tripId, string stopId)
    {
        var delay = 0;
        var feedEntity = GetFeedEntityById(tripId);
        if (feedEntity != null)
        {
            if (feedEntity.IsDeleted)
                return -1;
            
            var stopTimeUpdate = feedEntity.TripUpdate.StopTimeUpdates.Where(stu => stu.StopId == stopId).FirstOrDefault();
            if (stopTimeUpdate != null)
                delay = stopTimeUpdate.Departure.Delay;
        }
        
        var random = new Random((tripId + stopId).GetHashCode());
        
        if (random.Next(0, 1) == 0)
            delay += random.Next(1, 11);
        
        if (delay > 5)
            delay -= random.Next(1, 6);
        
        return delay;
    }
}