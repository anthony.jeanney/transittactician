using Microsoft.AspNetCore.Mvc;
using TransitTactitian.Core.Interfaces.Business;

namespace TransitTactician.API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TripController : ControllerBase
{
    private readonly ITripService _tripService;
    
    public TripController(ITripService tripService)
    {
        _tripService = tripService;
    }

    [HttpGet]
    [Route("tripsByNameAndTimes")]
    public IActionResult GetFullTripsByNameAndTime(String departure, String arrival, DateTime wantedArrivalTime)
    {
        return Ok(_tripService.GetFullTripsByNameAndTime(departure, arrival, wantedArrivalTime));
    }
    
    [HttpGet]
    [Route("trips")]
    public IActionResult GetTrips(int limit = 100)
    {
        return Ok(_tripService.GetTrips(limit));
    }
    
    [HttpGet]
    [Route("trips/{id}")]
    public IActionResult GetTrip(String id)
    {
        return Ok(_tripService.GetTripById(id));
    }
    
    [HttpGet]
    [Route("routes")]
    public IActionResult GetRoutes(int limit = 100)
    {
        return Ok(_tripService.GetRoutes(limit));
    }
    
    [HttpGet]
    [Route("routes/{id}")]
    public IActionResult GetRoute(String id)
    {
        return Ok(_tripService.GetRouteById(id));
    }
    
    [HttpGet]
    [Route("calendarDates")]
    public IActionResult GetCalendarDates(int limit = 100)
    {
        return Ok(_tripService.GetCalendarDates(limit));
    }
    
    [HttpGet]
    [Route("calendarDates/{id}")]
    public IActionResult GetCalendarDate(String id)
    {
        return Ok(_tripService.GetCalendarDateById(id));
    }
    
    [HttpGet]
    [Route("stopTimes")]
    public IActionResult GetStopTimes(int limit = 100)
    {
        return Ok(_tripService.GetStopTimes(limit));
    }
    
    [HttpGet]
    [Route("stopTimes/id")]
    public IActionResult GetStopTime(String tripId = "", String stopId = "")
    {
        if (tripId == "" && stopId == "")
            return BadRequest();
        if (tripId != "")
            return Ok(_tripService.GetStopTimeByTripId(tripId));
        return Ok(_tripService.GetStopTimeByStopId(stopId));
    }
    
    [HttpGet]
    [Route("stops")]
    public IActionResult GetStops(int limit = 100)
    {
        return Ok(_tripService.GetStops(limit));
    }
    
    [HttpGet]
    [Route("stops/{id}")]
    public IActionResult GetStop(String id)
    {
        return Ok(_tripService.GetStopById(id));
    }
    
    [HttpGet]
    [Route("stops/name/{name}")]
    public IActionResult GetStopsByName(String name)
    {
        return Ok(_tripService.GetStopsByName(name));
    }
    
    [HttpGet]
    [Route("feedEntities")]
    public IActionResult GetFeedEntities(int limit = 100)
    {
        return Ok(_tripService.GetFeedEntities(limit));
    }
    
    [HttpGet]
    [Route("feedEntities/{id}")]
    public IActionResult GetFeedEntity(String id)
    {
        return Ok(_tripService.GetFeedEntityById(id));
    }
    
    [HttpGet]
    [Route("predictedLateness")]
    public IActionResult GetPredictedLateness(string tripId, string stopId)
    {
        return Ok(_tripService.GetPredictedLateness(tripId, stopId));
    }
}