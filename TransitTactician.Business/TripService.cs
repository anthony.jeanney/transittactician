﻿using GTFS.Entities;
using GTFS.Entities.Enumerations;
using TransitRealtime;
using TransitTactician.Entities;
using TransitTactitian.Core.Interfaces.Business;
using TransitTactitian.Core.Interfaces.Repositories;

namespace TransitTactician.Business;

public class TripService : ITripService
{
    private readonly ITripRepository _tripRepository;
    
    public TripService(ITripRepository tripRepository)
    {
        _tripRepository = tripRepository;
    }
    
    public IEnumerable<Trip> GetTrips(int limit = 100)
    {
        if (limit > 100)
            limit = 100;
        return _tripRepository.GetTrips().Take(limit);
    }
    
    public Trip GetTripById(string tripId)
    {
        return _tripRepository.GetTripById(tripId);
    }
    
    public IEnumerable<Route> GetRoutes(int limit = 100)
    {
        if (limit > 100)
            limit = 100;
        return _tripRepository.GetRoutes().Take(limit);
    }
    
    public Route GetRouteById(string routeId)
    {
        return _tripRepository.GetRouteById(routeId);
    }
    
    public IEnumerable<CalendarDate> GetCalendarDates(int limit = 100)
    {
        if (limit > 100)
            limit = 100;
        return _tripRepository.GetCalendarDates().Take(limit);
    }
    
    public IEnumerable<CalendarDate> GetCalendarDateById(string calendarDateId)
    {
        return _tripRepository.GetCalendarDateById(calendarDateId);
    }
    
    public IEnumerable<Stop> GetStops(int limit = 100)
    {
        if (limit > 100)
            limit = 100;
        return _tripRepository.GetStops().Take(limit);
    }
    
    public Stop GetStopById(string stopId)
    {
        return _tripRepository.GetStopById(stopId);
    }
    
    public IEnumerable<Stop> GetStopsByName(string stopName)
    {
        return _tripRepository.GetStopsByName(stopName);
    }
    
    public IEnumerable<StopTime> GetStopTimes(int limit = 100)
    {
        if (limit > 100)
            limit = 100;
        return _tripRepository.GetStopTimes().Take(limit);
    }
    
    public IEnumerable<StopTime> GetStopTimeByTripId(string tripId)
    {
        return _tripRepository.GetStopTimeByTripId(tripId);
    }
    
    public IEnumerable<StopTime> GetStopTimeByStopId(string stopId)
    {
        return _tripRepository.GetStopTimeByStopId(stopId);
    }
    
    public IEnumerable<FeedEntity> GetFeedEntities(int limit = 100)
    {
        if (limit > 100)
            limit = 100;
        return _tripRepository.GetFeedEntities().Take(limit);
    }
    
    public FeedEntity GetFeedEntityById(string feedEntityId)
    {
        return _tripRepository.GetFeedEntityById(feedEntityId);
    }

    public IEnumerable<FullTrip> GetFullTripsByNameAndTime(String departure, String arrival, DateTime wantedArrivalTime)
    {
        // TODO: this only finds trips for the current day, need to find a way to get trips for the next days (use calendarDates ?)
        IEnumerable<Stop> departureStops = GetStopsByName(departure);
        IEnumerable<Stop> arrivalStops = GetStopsByName(arrival);
        List<FullTrip> fullTrips = new();
        foreach (Stop departureStop in departureStops)
        {
            foreach (Stop arrivalStop in arrivalStops)
            {
                IEnumerable<StopTime> departureStopTimes = GetStopTimeByStopId(departureStop.Id);
                IEnumerable<StopTime> arrivalStopTimes = GetStopTimeByStopId(arrivalStop.Id);
                foreach (StopTime departureStopTime in departureStopTimes)
                {
                    foreach (StopTime arrivalStopTime in arrivalStopTimes)
                    {
                        if (departureStopTime.TripId == arrivalStopTime.TripId)
                        {
                            Trip trip = GetTripById(departureStopTime.TripId);
                            Route route = GetRouteById(trip.RouteId);
                            FeedEntity feedEntity = GetFeedEntityById(trip.Id);
                            IEnumerable<CalendarDate> calendarDates = GetCalendarDateById(trip.ServiceId);
                            
                            // get all dates for this trip from the calendarDates
                            foreach (CalendarDate calendarDate in calendarDates)
                            {
                                // if the exception type is 2 (Removed), it means that the trip is not available on this date
                                if (calendarDate.ExceptionType.CompareTo(ExceptionType.Removed) == 0)
                                {
                                    continue;
                                }
                                
                                // if arrival time is before departure time, it means that this is a reverse trip
                                if (departureStopTime.ArrivalTime == null || departureStopTime.DepartureTime == null ||
                                    arrivalStopTime.ArrivalTime == null || arrivalStopTime.DepartureTime == null)
                                    continue;
                            
                                DateTime departureStopDateTime = new(calendarDate.Date.Year, calendarDate.Date.Month, calendarDate.Date.Day, 
                                    departureStopTime.DepartureTime.Value.Hours, departureStopTime.DepartureTime.Value.Minutes, departureStopTime.DepartureTime.Value.Seconds);
                            
                                DateTime arrivalStopDateTime = new(calendarDate.Date.Year, calendarDate.Date.Month, calendarDate.Date.Day, 
                                    arrivalStopTime.ArrivalTime.Value.Hours, arrivalStopTime.ArrivalTime.Value.Minutes, arrivalStopTime.ArrivalTime.Value.Seconds);
                                
                                DateTime currentTime = DateTime.Now;
                                //DateTime currentTime = new(2024, 1, 13, 10, 20, 0);
                                
                                Boolean isBeforeWantedTime = wantedArrivalTime.CompareTo(arrivalStopDateTime) > 0;
                                Boolean isCorrectOrder = departureStopDateTime.CompareTo(arrivalStopDateTime) < 0;
                                Boolean isNotTooEarly = wantedArrivalTime >= departureStopDateTime.AddHours(-24);
                                Boolean isAfterCurrentTime = departureStopDateTime.CompareTo(currentTime) > 0;
                                
                                if (isBeforeWantedTime && isNotTooEarly && isAfterCurrentTime && isCorrectOrder)
                                {
                                    var predictedLateness = GetPredictedLateness(trip.Id, departureStop.Id);
                                    fullTrips.Add(new FullTrip(departureStopTime, departureStop, arrivalStopTime, arrivalStop, trip, route, calendarDate, feedEntity, predictedLateness));
                                }
                            }
                        }
                    }
                }
            }
        }
        
        // sort
        fullTrips.Sort((x, y) => y.arrivalStopTime.ArrivalTime.Value.CompareTo(x.arrivalStopTime.ArrivalTime.Value));
        
        return fullTrips;
    }
    
    public int GetPredictedLateness(string tripId, string stopId)
    {
        return _tripRepository.GetPredictedLateness(tripId, stopId);
    }
}