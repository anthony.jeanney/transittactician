using System.Collections;
using GTFS.Entities;
using TransitRealtime;

namespace TransitTactitian.Core.Interfaces.Repositories;

public interface ITripRepository
{
    public IEnumerable<Trip> GetTrips();
    public Trip GetTripById(string tripId);
    public IEnumerable<Route> GetRoutes();
    public Route GetRouteById(string routeId);
    public IEnumerable<Stop> GetStops();
    public Stop GetStopById(string stopId);
    public IEnumerable<Stop> GetStopsByName(string stopName);
    public IEnumerable<StopTime> GetStopTimes();
    public IEnumerable<StopTime> GetStopTimeByTripId(string tripId);
    public IEnumerable<StopTime> GetStopTimeByStopId(string stopId);
    public IEnumerable<Agency> GetAgencies();
    public Agency GetAgencyById(string agencyId);
    public IEnumerable<CalendarDate> GetCalendarDates();
    public IEnumerable<CalendarDate> GetCalendarDateById(string calendarDateId);
    public IEnumerable<FeedEntity> GetFeedEntities();
    public FeedEntity GetFeedEntityById(string feedEntityId);
    public int GetPredictedLateness(string tripId, string stopId);
}