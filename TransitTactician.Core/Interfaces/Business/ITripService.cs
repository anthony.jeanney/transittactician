using System.Collections;
using GTFS.Entities;
using TransitRealtime;
using TransitTactician.Entities;

namespace TransitTactitian.Core.Interfaces.Business;

public interface ITripService
{
    public IEnumerable<Trip> GetTrips(int limit = 100);
    public Trip GetTripById(string tripId);
    public IEnumerable<Route> GetRoutes(int limit = 100);
    public Route GetRouteById(string routeId);
    public IEnumerable<CalendarDate> GetCalendarDates(int limit = 100);
    public IEnumerable<CalendarDate> GetCalendarDateById(string calendarDateId);
    public IEnumerable<Stop> GetStops(int limit = 100);
    public Stop GetStopById(string stopId);
    public IEnumerable<Stop> GetStopsByName(string stopName);
    public IEnumerable<StopTime> GetStopTimes(int limit = 100);
    public IEnumerable<StopTime> GetStopTimeByTripId(string tripId);
    public IEnumerable<StopTime> GetStopTimeByStopId(string stopId);
    public IEnumerable<FeedEntity> GetFeedEntities(int limit = 100);
    public FeedEntity GetFeedEntityById(string feedEntityId);
    public IEnumerable<FullTrip> GetFullTripsByNameAndTime(string departure, string arrival, DateTime wantedArrivalTime);
    public int GetPredictedLateness(string tripId, string stopId);
}